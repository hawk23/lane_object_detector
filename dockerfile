FROM python:3
RUN apt-get update
RUN apt install -y vim
RUN pip install opencv-contrib-python
RUN rm -rf /workspace/*
RUN mkdir -p /workspace/
COPY src/ /workspace/src
WORKDIR /workspace
