import argparse

parser = argparse.ArgumentParser(description='Object and Lane Detector demo')

# Object Detector specific args
parser.add_argument('--objects', action='store_true',
                    help='Run the object detection module')
parser.add_argument('--configFile', type=str, default='../in/yolo_v2.cfg',
                    help='Model config file. refer darnet format')
parser.add_argument('--weightsFile', type=str, default='../in/yolov2.weights',
                    help='Model weights file')
parser.add_argument('--inSize', type=tuple, default=(608, 608),
                    help='Input Size of frame for Object Detection model')
parser.add_argument('--labelsFile', type=str, default='coco.names',
                    help='a text file containing class names with each name on one line')
parser.add_argument('--scale', type=float, default=1/255.0,
                    help='Value to scale the input pixels with')
parser.add_argument('--cutoffProb', type=float, default=0.5,
                    help='class probability value above which all detection are considered for processing')
parser.add_argument('--thresholdIOU', type=float, default=0.4,
                    help='thrshold value for IOU during Non-maximal suppression')                   

# Lane Detector specific args
parser.add_argument('--lanes', action='store_true',
                    help='Run the lane detection module')

# Miscellaneous args
parser.add_argument('--videoInput', type=str, default='../in/test0.mp4',
                    help='Input Video File')
parser.add_argument('--videoOutput', type=str, default='../out/output.avi',
                    help='Ouput Video File')
parser.add_argument('--dump', action='store_true',
                    help='Dumps output Video File')
parser.add_argument('--display', action='store_true',
                    help='Displays output on the fly')

args = parser.parse_args()



