from abc import ABCMeta, abstractmethod

'''
'''
class ModuleBase(metaclass=ABCMeta):
    '''
    Abstract Base Class for represeting Modules. Any execution unit with the property of 
    having below two methods should subclass this class and implement the member functions.
    '''
    @abstractmethod
    def __call__(self, executeInput):
        '''
        Should Execute the Module computation
        '''
        pass

    @abstractmethod
    def draw(self, drawInput):
        '''
        Should execute the drawing functionality
        '''
        pass
