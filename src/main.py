import cv2
from options import args
from modulebase import ModuleBase
from lanedetect import LaneDetector
from objectdetect import ObjectDetector

# Some constants
FRAME_DELAY = 25
FPS = 24.0

if __name__ == '__main__':
    
    videoIn = cv2.VideoCapture(args.videoInput)
    if videoIn.isOpened():
        frameWidth  = videoIn.get(3)
        frameHeight = videoIn.get(4)
    else:
        raise RuntimeError("Unable to open video {0}".format(args.videoInput))
    
    # Create Modules list. This is collection of module objects which can later be executed.
    modulesList = []
    if args.lanes:
        detectLane = LaneDetector([frameWidth, frameHeight], args)
        modulesList.append(detectLane)
    if args.objects:
        detectObject = ObjectDetector([frameWidth, frameHeight], args)
        modulesList.append(detectObject)
    
    if not modulesList:
        raise RuntimeError("No modules to execute. Please check the arguments to the program")
    
    if args.dump:
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        videoOut = cv2.VideoWriter(args.videoOutput, fourcc, FPS, (int(frameWidth), int(frameHeight)))

    while videoIn.isOpened():
        ret, frameIn = videoIn.read()
        frameInCopy = frameIn
        if ret == False:
            break
            
        # Check if the module instances are of type ModuleBase
        for module_ in modulesList:
            if isinstance(module_, ModuleBase) is False:
                  raise TypeError("{0} is not of type ModuleBase".format(module_))

        '''
        Execute Modules. Currently this is serialized. Based on dependencies between modules we can 
        run them in parallel. Specifically in case of auxilary processors like GPU, we can potentially kick-off
        the job and return to the program for further execution asynchronously
        '''
        for module_ in modulesList:
            module_(frameIn)
        # call Module draw to visualize the output. This is pipelined.
        for module_ in modulesList:
            frameIn = module_.draw(frameIn)
        
        # let us not stall the output. Just display/dump the original frame
        if frameIn is None:
            frameIn = frameInCopy
        if args.dump:
            videoOut.write(frameIn)
        if args.display and frameIn is not None:
            cv2.imshow('output', frameIn)
            if cv2.waitKey(FRAME_DELAY) & 0xFF == ord('q'):
                break
        
    videoIn.release()
    cv2.destroyAllWindows()
            
