from modulebase import ModuleBase
import numpy as np
import cv2

class LaneDetector(ModuleBase):
    '''
    A lane detection module. The core is based on detecting lines using Hough transform.
    I do some pre processing like region carving (basically zeroing out unnecessary regions)
    color thresholding etc. followed by a Canny edge on the input frame followed by a Hough transform
    which is followed by a small post processing where I suppress certain lines based on their slopes.
    This is followed by drawing the line on the input frame.
    
    Note: The code for carving out unnecessary region (in preprocess_image) is very much
    camera placement dependent. I have tried the code on videos where everything goes haywire because
    the region carving completely ignores the lanes. This is particularly suited for Udacity test videos.
    '''

    '''
    Some Constants. We could perhaps expose these to the client but I believe that is 
    too low level control
    '''
    RHO_ACCURACY    = 1
    THETA_ACCURACY  = np.pi/90 
    MIN_VOTES       = 20
    MIN_LINE_LENGTH = 20
    MAX_LINE_GAP    = 5
    
    YELLOW_LOW  = np.asarray([20, 100, 100]) 
    YELLOW_HIGH = np.asarray([30, 255, 255])
    WHITE_LOW   = np.asarray([0, 0, 230]) 
    WHITE_HIGH  = np.asarray([255, 80, 255])

    def __init__(self, frameProps, args):
        if frameProps[0] <= 0 or frameProps[1] <= 0:
            raise ValueError("Invalid value for frame width or frame height")
        
        self.m_FrameWidth = frameProps[0]
        self.m_FrameHeight = frameProps[1]
        
        self.m_RightLane = []
        self.m_LeftLane = []

    def __call__(self, frameIn):
        '''
        Executes the core algorithm. Runs preprocessing, Hough Transform and postprocessing to collect a 
        list of possible candidates for lanes. This call stores the results to be used by draw function.
        
        :param frameIn: Input frame. A numpy array of shape (height, width, 3)
        '''
        if frameIn.shape != (int(self.m_FrameHeight), int(self.m_FrameWidth), 3):
            raise ValueError("Invalid Value of frame Size. Got {0} but was expecting {1}".format((int(self.m_FrameHeight), int(self.m_FrameWidth), 3), frameIn.shape))

        frameIn = self._preprocessFrame(frameIn)
        linesList = self.__class__._getLinesList(frameIn)
        
        if np.any(np.equal(linesList, None)):
            return
        else:
            self._processLines(linesList)
      
    def draw(self, frameIn):
        '''
        Takes a list of candidates for lane lines and draws the right and left lane on to the input frame and returns it.
        
        :param frameIn: Input frame
        :ret frameIn: A frame which contains the left and right lanes drawn on it
        '''
        cls = self.__class__
        if len(self.m_RightLane) is not 0:
            rightLane = cv2.fitLine(np.asarray(self.m_RightLane), cv2.DIST_L1, 0, 0.01, 0.01)
                
            y1 = self.m_FrameHeight
            x1 = cls._getx(y1, rightLane)
            y2 = self.m_FrameHeight * 0.65
            x2 = cls._getx(y2, rightLane)
            if x1 is None or x2 is None:
                return

            cv2.line(frameIn, (int(x1), int(y1)), (int(x2), int(y2)), (0,255,0), 5)
            
        # Draw the left lane on the original frame
        if len(self.m_LeftLane) is not 0:
            leftLane  = cv2.fitLine(np.asarray(self.m_LeftLane), cv2.DIST_L1, 0, 0.01, 0.01)
            
            y1 = self.m_FrameHeight
            x1 = cls._getx(y1, leftLane)
            y2 = self.m_FrameHeight * 0.65
            x2 = cls._getx(y2, leftLane)
            if x1 is None or x2 is None:
                return

            cv2.line(frameIn, (int(x1), int(y1)), (int(x2), int(y2)), (0,255,0), 5)
            
        return frameIn
            
    def _preprocessFrame(self, frameIn):
        '''
        Do some preprocessing on the input frame. Discard not needed colors (Lane lines are mostly white or yellow).
        Get the edge image and carve out unnecessary region in the frame. Note that the carve out region depends on
        the placement of camera.
    
        :param frameIn: Input frame
    
        :ret frameEdge: preprocessed frame
        '''
        
        frameHSV = cv2.cvtColor(frameIn, cv2.COLOR_BGR2HSV)
        frameThresholded = self.__class__._colorThreshold(frameIn, frameHSV) 
        frameHSV = cv2.split(frameThresholded)
        frameGrey = frameHSV[2]
        
        # Carve out unnecessary region in the frame
        pts = np.array([[(0,0), (0, self.m_FrameHeight), (self.m_FrameWidth/2 - 25, self.m_FrameHeight/2 + 25), (self.m_FrameWidth/2 + 25, self.m_FrameHeight/2 + 25), (self.m_FrameWidth, self.m_FrameHeight), (self.m_FrameWidth, 0)]], 'int32')
        #pts = np.array([[(0,0), (0, m_FrameHeight), (m_FrameWidth/4, m_FrameHeight/3 + 20), (0.6*m_FrameWidth, m_FrameHeight/2 + 10), (m_FrameWidth, m_FrameHeight), (m_FrameWidth, 0)]], 'int32')
        cv2.fillPoly(frameGrey, pts, 0)  
        frameEdge = cv2.Canny(frameGrey, 50, 150, apertureSize=3)
        # cv2.imshow('w', frameEdge)
        
        return frameEdge 
        
    def _processLines(self, linesList):
        '''
        Distinguish points into right lane, left lane and neither(ignore)
    
        :param linesList: A list of lines represented by two points. Basically the output of cv2.HoughLinesP
        '''

        # reset for every call
        self.m_RightLane = []
        self.m_LeftLane  = []
    
        for lines in linesList:
            slope = self.__class__._getSlope(lines[0], lines[1], lines[2], lines[3])
            if slope == None:
                return 
            # These lines would not be on either lanes.
            if -0.2 < slope < 0.2:
                return 
            # Collect points adding to right lane (potentially)
            if slope > 0:
                self.m_RightLane.append((lines[0], lines[1]))
                self.m_RightLane.append((lines[2], lines[3]))
            # Collect points adding to the left lane (potentially)
            else:
                self.m_LeftLane.append((lines[0], lines[1]))
                self.m_LeftLane.append((lines[2], lines[3]))
                
    @classmethod
    def _colorThreshold(cls, frameIn, frameHSV):
        '''
        Color thresholding on the input image. Black out the pixels which do not fall in the 
        range of yellow and white thresholds
    
        :param frameIn: Input frame
        :param frameHSV: Input frame in hsv space
    
        :ret threshold: The thresholded frame 
        '''
    
        yellowMask = cv2.inRange(frameHSV, cls.YELLOW_LOW, cls.YELLOW_HIGH)
        whiteMask  = cv2.inRange(frameHSV, cls.WHITE_LOW, cls.WHITE_HIGH)
        mask = cv2.bitwise_or(yellowMask, whiteMask)
        threshold = cv2.bitwise_and(frameIn, frameIn, mask=mask)
    
        return threshold
                
    @classmethod
    def _getLinesList(cls, frameIn):
        '''
        Use Hough transform to get Lines in terms of point coordinates
        
        :param frameIn: Input frame
    
        :ret linesList: A list of Lines (in terms of  point coordinates)
        '''
    
        linesList = cv2.HoughLinesP(frameIn, cls.RHO_ACCURACY, cls.THETA_ACCURACY, cls.MIN_VOTES, cls.MIN_LINE_LENGTH, cls.MAX_LINE_GAP)
        # omit the first dimension
        linesList = np.squeeze(linesList, 1)
    
        return linesList
        
    @classmethod    
    def _getx(cls, y, lane):
        '''
        Get the x coordinate give line and y coordinate.
       
        :param y: y coordinate of the point whose x coordinate needs to be calculated
        :param lane: A list of four elements [x1,y1,x2,y2] where (x1,y1) is a vector colliniear to the given line
                     and (x2,y2) is a point on the line. Basically output of cv2.fitLine
    
        :ret: x coordinate of the point
        :ret None: In case the slope is zero. It is not a problem as we do not consider lines of slope zero for our lanes.
        '''
        
        m, c = cls._getLineProps(lane)
    
        if m is not 0 and not None:
            return ((y-c)/m)
        else:
            return None
    
    @staticmethod
    def _getSlope(x1, y1, x2, y2):
        '''
        Get slope of line
        
        :param x1, y1, x2, y2: 4 scalars with (x1,y1) and (x2,y2) as points
    
        :ret dy/dx : slope of the line represented by thr two points
        :ret None: In case there is a divide by zero
        '''
    
        if x2 != x1:
            dy = float(y2) - float(y1)
            dx = float(x2) - float(x1)
    
            return dy/dx
        else:
            return None
    
    @staticmethod
    def _getLineProps(line):
        '''
        Get the slope and intercept of the line defined by collinear vector and a point on the line
    
        :param line: A list of four elements [x1,y1,x2,y2] where (x1,y1) is a vector colliniear to the given line
                     and (x2,y2) is a point on the line. Basically output of cv2.fitLine
    
        :ret m,c: slope and intercept of the line passing through (x1,y1) and (x2,y2)
        '''
    
        if line[0] is not 0:
            m = (float(line[1])/line[0])
            c = float(line[3]) - (m * line[2])
        else:
            m = None
            c = None
    
        return m,c
