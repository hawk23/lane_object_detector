import numpy as np
import cv2
from modulebase import ModuleBase

class ObjectDetector(ModuleBase):
    '''
    An Object detector module. This class uses the opencv dnn module to do the inference.
    Particularly it supports the darknet module. It reads config file(model definition and other
    params) and weights file to create a network on which forward function can be called for execution.
    I am using couple of pretrained models(but, this is exposed through options and hence alterable) based
    on YOLO architecture, particularly YOLOv2 and YOLOv2_tiny.
    
    We get a list of bounding boxes(with mulitple anchors) and class probabilites. 
    We then extract potentially boxes based on cutoff probability on the classes and then do a 
    non maximal suppression on overlapping boxes. The resulting boxes are then drawn on to the frame along
    with their class names and class probabilities
    
    I am using models pretrained on COCO dataset which contains 80 classes. Refer file 'coco.names' for the 
    class list
    '''
    def __init__(self, frameProps, args):
        if frameProps[0] <= 0 or frameProps[1] <= 0:
            raise ValueError("Invalid value for frame width or frame height")
            
        self.m_FrameWidth = frameProps[0]
        self.m_FrameHeight = frameProps[1]
        self.args = args
        
        self.m_ObjDetectNet = cv2.dnn.readNetFromDarknet(args.configFile, args.weightsFile)
        self.m_OutLayer = self.m_ObjDetectNet.getUnconnectedOutLayersNames()
        
        self.m_FrameInSize = args.inSize
        
        # Load class names and colors for drawing after detection
        self.m_Labels = open(args.labelsFile).read().strip().split("\n")
        if not self.m_Labels:
            raise ValueError("Cannot have empty class labels")
        self.m_Colors = np.random.randint(0, 255, (len(self.m_Labels), 3), 'uint8')
        
    def __call__(self, frameIn):
        '''
        Executes one forward pass on frameIn. Candidate bounding boxes, class probabilities and
        correspoding class IDs are collectde to be use din drawing operation
        
        :param: frameIn Input frame. A numpy array of shape (height, width, 3)
        '''
        if frameIn.shape != (int(self.m_FrameHeight), int(self.m_FrameWidth), 3):
            raise ValueError("Invalid Value of frame Size. Got {0} but was expecting {1}".format((int(self.m_FrameHeight), int(self.m_FrameWidth), 3), frameIn.shape))

        frameBlob = cv2.dnn.blobFromImage(frameIn, self.args.scale, self.m_FrameInSize, swapRB = True)
        self.m_ObjDetectNet.setInput(frameBlob)
        netOut = self.m_ObjDetectNet.forward(frameIn)
        
        self.m_ClassProb = []
        self.m_ClassID = []
        self.m_Boxes = []
        self.m_Indices = []

        for out in netOut:
            for detection in out:
                probs  = detection[5:]
                maxClass = np.argmax(probs)
                maxProb = probs[maxClass]
                if maxProb > self.args.cutoffProb:
                    centerX = int(detection[0] * self.m_FrameWidth)
                    centerY = int(detection[1] * self.m_FrameHeight)
                    boxWidth = int(detection[2] * self.m_FrameWidth)
                    boxHeight = int(detection[3] * self.m_FrameHeight)
                    topleftX = centerX -  int(boxWidth/2)
                    topleftY = centerY - int(boxHeight/2)
                    
                    self.m_ClassProb.append(float(maxProb))
                    self.m_ClassID.append(maxClass)
                    self.m_Boxes.append([topleftX, topleftY, boxWidth, boxHeight])
                    
        self.m_Indices = cv2.dnn.NMSBoxes(self.m_Boxes, self.m_ClassProb, self.args.cutoffProb, self.args.thresholdIOU)
        
    def draw(self, frameIn):
        '''
        Given a list of bouding boxes, class probs and classIDs. draws the bounding box and writes
        text(class name and class prob)

        :param frameIn: Input frame
        :ret frameIn: A frame which contains bounding boxes drawn and class ID and prob written on them
        '''
        if len(self.m_Indices) == 0:
            return
            
        for i in self.m_Indices:
            i = i[0]
            x = self.m_Boxes[i][0]
            y = self.m_Boxes[i][1]
            w = self.m_Boxes[i][2]
            h = self.m_Boxes[i][3]
            
            color = list(map(int, self.m_Colors[self.m_ClassID[i]]))
            cv2.rectangle(frameIn, (x, y), (x + w, y + h), color, 3)
            text = "{0}: {1:.3f}".format(self.m_Labels[self.m_ClassID[i]], self.m_ClassProb[i])
            cv2.putText(frameIn, text, (x, y-5), cv2.FONT_ITALIC, 0.5, color, 1)
          
        return frameIn
