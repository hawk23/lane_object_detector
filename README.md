### WHAT IS THIS?
It is a toy program to detect lanes and objects. It has two modules one for object detection (car, person, truck etc.) and another one for lane lines detection.

### HOW TO USE?
1. cd into src. You will find a file called commands.sh. This essentially invokes the main program with arguments. This program takes in video as input and produces video output. Run sh commands.sh and you should see the output displayed.

2. The above is a basic run. You can go to the following link to download more videos and model files [here.](https://drive.google.com/drive/folders/1kHefujscKviI2MXvvehXO7lHehcqBmgo?usp=sharing) Unzip and copy all the contents(not folder) into "in" folder.

3. Now you can have a look at options.py file to change the inputs and other settings.
You can run python main.py with other settings (look at commands.sh file for example command lines). For small videos I suggest to use dump option.

### DEPENDENCIES:
1. python3.6 or later
   * sudo add-apt-repository ppa:deadsnakes/ppa
   * sudo apt update
   * sudo apt install python3.6

2. opencv4.0.0 (including contrib)
   * pip install opencv-contrib-python
   
Make sure your pip points to the right python version(which is python3.6)

### DOCKER:
If you have docker installed then life is simpler :)
Just run sh usedocker.sh. the script basically builds the docker image from dockerfile and starts the container. You will now be in the container. Now follow steps 1-3 from HOW TO USE SECTION. Just one catch use the --dump option instead of --display option due to X server. You can edit the commands.sh for the same (vim will already be installed). When you dump, the program output can be found in "out" folder

### WHAT CAN BE DONE BETTER?
1) This program is horribly slow and the perf decreases with addition of more modules. Modules like ObjectDetector can be run on GPU for faster processing. Current program does not support that.
2) The Lanedetector is heaviliy dependent on camera position. It would be really better if that dependency is removed (Look next section)
3) The ObjectDetector does not work well on cars which are far in the frame as it was trained on images with car in focus. A minor fine tuning with additional data should get it working well.
4) Code: I pass args to modules and this leads to unnecessary reveal of parameters not needed by module. This can be solved by segregating the args in the client(main).

### NOTE:
1. View all executbale units as modules. Create modules and collect them and then execute and perform other operations. In this case we have two modules, LaneDetector and ObjectDetector both of which subclass ModuleBase. ModuleBase is abstract base class which all modules need to inherit. We build a queue of module objects and then execute them. The code has some more explanation on algorithms.
2. The lane detector currently has one problem that it is very much restricted by camera position. So it does not work well in all cases. When you download additional data from the above link, I suggest to use test1.mp4, test2.mp4 for lane detection as it was caliberated for that camera position. On videos where the camera position is different, the results will be bad.
A couple of blogs on this:
https://buzzrobot.com/thinking-in-the-hough-space-the-thinking-ea56bd2a8014
https://buzzrobot.com/thinking-in-the-hough-space-the-doing-d17a13e068fe
3. For Object Detection I borrowed cfg files and weight files from [darknet.](https://github.com/pjreddie/darknet)
